#!/bin/bash
rm -rf ./redirects #delete old redirects folder and any files within if it exists
mkdir -p ./redirects #re-make it, now empty
tr -d '\r' < ./_data/models.txt > models_tmp.txt #prepare models.txt for reading, fixing any line endings
while IFS="" read -r p || [ -n "$p" ] #read each line from models.txt
do
  cp ./_data/MODEL_hmm.html "./redirects/$p hmm.html" #clone template hmm.html
  sed -i -e "s/MODEL/$p/g" "./redirects/$p hmm.html" #edit new hmm.html
  cp ./_data/MODEL_hmm.html "./redirects/$p frubom.html" #clone template frubom.html
  sed -i -e "s/MODEL/$p/g" "./redirects/$p frubom.html" #edit new frubom.html
done < models_tmp.txt
rm models_tmp.txt #delete temp cleaned file