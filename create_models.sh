#!/bin/bash
mkdir -p ./models
tr -d '\r' < ./_data/models.txt > models_tmp.txt
while IFS="" read -r p || [ -n "$p" ]
do
  cp template.html ./models/$p.html
  sed -i -e "s/synapse/$p/g" ./models/$p.html
done < models_tmp.txt
rm models_tmp.txt